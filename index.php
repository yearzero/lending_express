<?php
session_start();
$GLOBALS['visited'] = isset($_SESSION['visited']);


//requiring my .env file, this is not in git and will be sent
require 'autoload_env.php';
//a global autoloader per class requested per respective namespace
require 'global_autoload.php';
////requiring vendor package (Encrypt)
require 'vendor/autoload.php';

//my globals are fixed as I am rooting from index
$scheme = $_SERVER['REQUEST_SCHEME'];
$host = $_SERVER['HTTP_HOST'];
$home = $_SERVER['REQUEST_URI'];
$uri_parts = explode('?', $home, 2);
$home_url = "$scheme://$host" . $uri_parts[0];
$queryString = $_SERVER['QUERY_STRING'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $home_url ?>public/css/main.css">
</head>
<body>

<?php
//conditional URL page switcher
require 'views/header.php';
if (!array_key_exists(1, $uri_parts)) {
    require 'views/users.php';
} else {
    switch ($uri_parts[1]) {
        case 'users':
            require 'views/users.php';
            break;
        case 'stats':
            require 'views/stats.php';
            break;
        default:
            return require 'views/users.php';
    }
}
?>
<script src="public/js/jquery341.js"></script>

<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous">
</script>
<script src="<?php echo $home_url ?>public/js/echarts-en.simple.min.js"></script>

<script>
  const currentUrl = window.location.search.substr(1);
  // creating a prototypical max function that I can call globally
  // (basically adds a function to "Array" Object)
  Array.prototype.max = function() {
    return Math.max.apply(null, this);
  };
  var userAgeArr;
  $(document).ready(function () {


    //invoke the datatable instance
    if (currentUrl === 'stats') {
      $('#table_stats').DataTable();
    } else {
      $('#table_users').DataTable();
    }

    //getting .active on current url
    $('.navbar-nav').children().each(function (index, element) {
      if (currentUrl === element.innerText.toLowerCase()) {
        $(element).addClass('active');
      }
    });

    //invoke the echart instance
  });

  //echart is an IIFE due to JQuery conflicts.
  (function getEchart() {
    if (currentUrl !== 'stats') {
      return false;
    }
    //adding oldest user from user age array
    userAgeArr = Object.keys(usersArr.usersByAge);
    $('.oldest-user').append(userAgeArr.max());

    var user_chart_city = echarts.init(document.getElementById('user_chart_city'));
    var user_chart_age = echarts.init(document.getElementById('user_chart_age'));
    // specify chart configuration item and data
    var cityChartOptions = {
      xAxis: {
        data: Object.keys(usersArr.usersPerCity)
      },
      yAxis: {},
      series: [{
        name: 'City',
        type: 'bar',
        data: Object.values(usersArr.usersPerCity)
      }]
    };
    var ageChartOptions = {
      xAxis: {
        data:Object.keys(usersArr.usersByAge),
      },
      yAxis: {},
      series: [{
        name: 'Age',
        type: 'bar',
        data:Object.values(usersArr.usersByAge),

      }]
    };

    user_chart_city.setOption(cityChartOptions);
    user_chart_age.setOption(ageChartOptions);
  })(currentUrl);
</script>
</body>
</html>


