<?php
$users = require dirname(__FILE__) . '/../workers/get-users.php';
?>
<div class="container">
    <table id="table_users" class="display">
        <thead class="text-capitalize">
        <tr>
            <th>first name</th>
            <th>last Name</th>
            <th>email</th>
            <th>birth date</th>
            <th>phone</th>
            <th>city</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if (!isset($users)){
            echo '<div class="text-danger">Error: no users fetched</div>';
        }else {
            foreach ($users as $key => $user) {
                echo "<tr>";
                echo "<td>$user->first_name</td>";
                echo "<td>$user->last_name</td>";
                echo "<td>$user->email</td>";
                echo "<td>$user->birth_date</td>";
                echo "<td>$user->phone</td>";
                echo "<td>$user->city</td>";
                echo "</tr>";
            }
        }
        ?>
        </tbody>
    </table>
</div>