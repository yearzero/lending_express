<?php
$stats = require dirname(__FILE__) . '/../workers/get-stats.php';
$db_stats = $stats->parseLog();
$users = $stats->userStats()
?>
<div class="container-fluid">
    <h2 class="text-center text-capitalize">user distribution by city</h2>
    <div id="user_chart_city" style="height:400px;width: 100%;"></div>
    <h2 class="text-center text-capitalize">user distribution by Age</h2>
    <div id="user_chart_age" style="height:400px;width: 100%;"></div>
    <h1 class="alert alert-warning text-capitalize oldest-user" role="alert">
        oldest user is
    </h1>
    <h2 class="text-center">DB Status</h2>
    <table id="table_stats" class="display">
        <thead class="text-capitalize">
        <tr>
            <th>time</th>
            <th>error type</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if (!isset($db_stats) || empty($db_stats)) {
            echo '<div class="text-warning">no DB stats to show ATM</div>';
        } else {
            foreach ($db_stats as $key => $db_stat) {
                echo "<tr>";
                echo "<td>" . date('F d, Y h:i:s A', $db_stat['time']) . "</td>";
                echo "<td>{$db_stat['error']}</td>";
                echo "</tr>";
            }
        }
        ?>
        </tbody>
    </table>
</div>

<script>
// get user response before chart data
  var usersArr = <?php echo json_encode($users); ?>;

</script>