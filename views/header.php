<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="<?php $home_url?>?users""><img src="<?php $home_url?>public/images/whiteLogo.png"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?php $home_url?>?users">Users</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php $home_url?>?stats">Stats</a>
            </li>
        </ul>
    </div>
</nav>
<div class="breaker-50"></div>