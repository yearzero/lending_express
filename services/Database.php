<?php

namespace Services;

class Database
{
    private $serverName;
    private $userName;
    private $password;
    private $dbname;
    private $charSet;
    private $pdo;

    /***
     * Initial singleton connection to DB
     * @return \PDO
     */
    public function connect()
    {
        $this->serverName = '194.213.4.152';
        $this->userName = 'lending-exam';
        $this->password = 'AbCdEfLending-Exam-2019-@xcv';
        $this->dbname = 'lending';
        $this->charSet = 'utf8';
        try {
            $dsn = "mysql:host=" . $this->serverName . ";dbname=" . $this->dbname . ";charset=" . $this->charSet;
            $pdo = new \PDO($dsn, $this->userName, $this->password);
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->pdo = $pdo;
            return $pdo;
        } catch (\Exception $e) {
            echo 'error connecting! Reason: ' . $e->getMessage();
        }
    }

    /**
     * Store procedure
     * @param $users
     */
    public function save($users)
    {
        $this->connect();
        $query = $this->pdo->prepare(
            "INSERT INTO users (
                first_name,last_name,email,birth_date,phone,city,cc
            ) VALUES (:first_name,:last_name,:email,:birth_date,:phone,:city,:cc)"
        );
        foreach ($users as $user) {
            if (!property_exists($user, 'cc')) {
                $user->cc = '000000000000000000000000';
            } else {
                $encryptor = new Encryptor;
                $encryptor->encrypt($user->cc);
            }
            try {
                $query->bindParam(':first_name', $user->first_name);
                $query->bindParam(':last_name', $user->last_name);
                $query->bindParam(':email', $user->email);
                $query->bindParam(':birth_date', $user->birth_date);
                $query->bindParam(':phone', $user->phone);
                $query->bindParam(':city', $user->city);
                $query->bindParam(':cc', $user->cc);
                $query->execute();
            } catch (\PDOException $e) {
                $logger = new Logger($e);
                continue;
            }
        }
    }

    /**
     * Listing Procedure
     * @return mixed
     */
    public function index()
    {
        try {
            $this->connect();
            $this->createTableUsers();
            $query = $this->pdo->prepare(
                "SELECT * from users"
            );
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_OBJ);
        } catch (\PDOException $e) {
            $logger = new Logger('INDEX ERROR', $e);
        }
    }

    public function createTableUsers()
    {
        try {
            $query = $this->pdo->prepare("
            CREATE TABLE IF NOT EXISTS users (
                id INT AUTO_INCREMENT PRIMARY KEY,
                first_name VARCHAR(50) NOT NULL,
                last_name VARCHAR(50) NOT NULL,
                email VARCHAR(100) NOT NULL,
                birth_date DATE NOT NULL,
                phone VARCHAR(15) NOT NULL,
                city VARCHAR(50) NOT NULL,
                cc VARCHAR(24) NOT NULL,
                UNIQUE KEY unique_email (email));
                ");
            $query->execute();
        } catch (\PDOException $e) {
            $logger = new Logger($e);
        }

    }
}