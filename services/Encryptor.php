<?php
namespace Services;
use PhpAes\Aes;

/**
 * Class Encryptor
 * Is a starter class for the AES encryption package,
 * this gives us a zero config environment to encrypt/decrypt,
 * I am __constructing both salt and pepper (iv) to increase clients credit card security
 * This should scale well as it's the thinest encyption package I've found with a normal and effecient complexity.
 * @package Services
 */
class Encryptor
{
    public function __construct()
    {
            $this->aes = new Aes(env('z'), env('mode'), env('iv'));
    }

    public function encrypt($string)
    {
        return $this->aes->encrypt($string);
    }

    public function decrypt($string)
    {
        return $this->aes->decrypt($string);
    }


}