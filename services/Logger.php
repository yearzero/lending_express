<?php

namespace Services;
/**
 * Class Logger, creates a db_log.txt on runtime.
 * @package Services
 */
class Logger
{
    public function __construct($errorObj)
    {
        $myfile = fopen("logs/db_log.txt", "a") or die("Unable to open file!");
        fwrite($myfile, "{\"time\":".time().",\"error\":"."\"".$errorObj->getMessage()."\"}\n");
        fclose($myfile);
    }
}