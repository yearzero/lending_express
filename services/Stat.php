<?php

namespace Services;
/**
 * Class Stat picks up the log and preforms
 * certain methods on it, such as parsing it for view
 * @package Services
 */
class Stat
{
    private $log;

    public function __construct()
    {
        $log = file_get_contents(__DIR__ . '/../logs/db_log.txt');
        $this->log = $log;
    }

    /**
     * Once the log is picked up and not empty,
     * I will loop over the exploded logfile and
     * parse it and serve to view file
     * @return array
     */
    public function parseLog()
    {
        $elaboratedErrors = [];

        if (!empty($this->log)) {
            $errors = explode("\n", $this->log);
            foreach ($errors as $error) {
                if (!empty($error)) {
                    $elaboratedErrors[] = json_decode($error, 1);
                }
            }
        }
        return $elaboratedErrors;
    }

    public function userStats()
    {
        $db = new Database;
        $userModel = new \Models\User;
        $usersPerCity = [];
        $usersByAge = [];
        $users = $db->index();

        foreach ($users as $user) {
            //treating user city
            if (!isset($usersPerCity[$user->city])) {
                $usersPerCity[$user->city] = 1;
            } else {
                $usersPerCity[$user->city]++;
            }
            $userAge = $userModel->getAge($user->birth_date);

            //treating user age
            if (!isset($usersByAge[$userAge])) {
                $usersByAge[$userAge] = 1;
            } else {
                $usersByAge[$userAge]++;
            }
        }
        return ['usersPerCity' => $usersPerCity, 'usersByAge' => $usersByAge];
    }
}