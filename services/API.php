<?php

namespace Services;
/**
 * Class API
 * becuase we constantly call Users => Class should implement a cache that checks
 * for immutable responses
 * Redis should be a good choice.
 * @package Services
 */
class API
{
    private $url = '';

    public function __construct($remote_host, $remote_key, $remote_pwd)
    {
        if (is_null($remote_pwd) || is_null($remote_key) || is_null($remote_host)) {
            return 'cannot start API service without destination';
        }

        $this->remote_host = $remote_host;
        $this->remote_key = $remote_key;
        $this->remote_pwd = $remote_pwd;
        $this->url = "$this->remote_host?key=$this->remote_key&password=$this->remote_pwd";

    }

    /**
     * calls endpoint
     * @return mixed|string
     */
    public function callEndpoint()
    {
        if ($this->url === ('' || null)){
            return 'url is truncated';
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $this->url);

        if (!curl_exec($curl)) {
            die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
        }

        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }


}