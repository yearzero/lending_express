<?php

namespace Models;

use Services\API;
use Services\Database;

require dirname(__FILE__) . '/../services/API.php';

class User
{
    private $remote_host = 'http://lending.weblife.co.il/api/users.php';
    private $remote_key = 'lending-express';
    private $remote_pwd = 'lending-express-2018';

    const userRuleSet = [
        'above_age' => ['default' => 18, 'haifa' => 21],
    ];


    /**
     * returning users JSON object
     * since API had invalid JSON, there is also file content retrieval (commented out)
     * @return mixed
     */
    public function getUsers()
    {

        //from file
//        $usersObj = json_decode(file_get_contents("users.json", FILE_USE_INCLUDE_PATH));

        //from API => works now and JSON is valid
        $response = new API($this->remote_host, $this->remote_key, $this->remote_pwd);
        $usersObj = json_decode($response->callEndpoint());
        $result = $this->appendUserRulesSet($usersObj);
        return $result;
    }

    /**
     * @param $usersObj
     * @return array
     */
    private function appendUserRulesSet($usersObj)
    {
        $userArray = $usersObj->users;
        $validUsers = [];

        foreach ($userArray as $key => $user) {
            $age = $this->getAge($user->birth_date);
            $city = strtolower($user->city);
            $ruleSet = self::userRuleSet['above_age'];

            //this way we can expand our $ruleSet to introduce new city rules more easily.
            //so if $city key exists in ruleSet, it will follow the ruleSet of that city
            $cityRestriction = array_key_exists($city, $ruleSet);
            $allowedAge = $cityRestriction ? $ruleSet[$city] : $ruleSet['default'];

            //laslty, we check the age if passes the allowed age int value of city key.
            if ($age > $allowedAge) {
                $validUsers[] = $user;
            }
        }
        if (empty($this->indexUsers())){
            $this->setUsers($validUsers);
        }
        return $this->indexUsers();
    }

    public function getAge($dob)
    {
        $now = new \DateTime('NOW');
        $dteStart = new \DateTime($dob);
        return $dteStart->diff($now)->y;
    }

    protected function setUsers($users)
    {
        $db = new Database;
        $db->save($users);
    }

    protected function indexUsers()
    {
        //SHOULD CREATE REDIS CACHE HERE
        // AND CHECK CONSISTENCY OF DATA BY CHKSUM
        $db = new Database;
        return $db->index();
    }
}