<?php
spl_autoload_register(function ($class) {
    $explodedClass = explode('\\', $class);
    if (sizeof($explodedClass) > 2) {
        return 'depth unreachable atm';
    }
    $reconstructedClass = strToLower($explodedClass[0]) . '/' . $explodedClass[1] . '.php';
    if (!file_exists($reconstructedClass)) {
        return false;
    }
    require $reconstructedClass;
});

