#Lending Express

![picture](https://i.imgur.com/RxKWEOy.gif)

Dear Lending team, please follow these steps:

## Project setup

1. `git clone` this repo, or download it to your local folder.
2. There may be PHP 7.1 and above functions in there, just FYI in terms of compiler.
3. CD into dir and Run `composer install`
4. please create a `env.php` and paste [this](https://pastebin.com/GpS55CuG) into the file.


##A Few Notes
- I tried to keep convention light, though all classes are fully callable due to `\global_autoload.php`
- `/services` holds app's services with public visibility. 
- `/models` holds app's models
- On runtime you are essentially  creating a procedure of: create, call, amend and display,
 it happens in one concurrent request.
- `/logs/db_log.txt` is the log file, I got confused, thought you guys wanted it also displayed 
(appearantly you just wanted it created) so it's on display on stats page.
- originally requested by you to create a `get-users.php`, I went ahead and created them as 'controllers',
 they are in `/workers` folder.
 - commented as much as I could.
 
 Thanks,
 Erez